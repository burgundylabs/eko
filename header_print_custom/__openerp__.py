{
    "name": "Custom External Header PDF Report",
    "version": "8.0",
    "depends": ['report'],
    "author": "Joenan",
    "category": "Sales",
    "description" : """Custom External Header PDF Report""",
    'data': [
            'views/report_header.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
