# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round
import openerp.addons.decimal_precision as dp
import math
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class product_product(osv.osv):
    _inherit = "product.product"

    def _get_detail_qty(self, x, mod_factor, decimal):
        qty = x * mod_factor
        (mod_qty, res_qty) = divmod(qty, mod_factor)
#         decimal = len(str(int(res_qty)))
        res_qty = res_qty / 10 ** decimal
        y = mod_qty + res_qty
        return y
    
    def _product_available_detail(self, cr, uid, ids, field_names=None, arg=False, context=None):
        res = {}
        for product in self.browse(cr, uid, ids, context):
            if product.uom_id.category_id.detail_qty and product.uom_id.uom_type != 'reference' and product.type != 'service':
                decimal = math.log(product.uom_id.rounding,0.1)
                qty_available_detail = float_round(self._get_detail_qty(product.qty_available, product.uom_id.factor_inv, decimal), precision_rounding=product.uom_id.rounding)
                virtual_available_detail = float_round(self._get_detail_qty(product.virtual_available, product.uom_id.factor_inv, decimal), precision_rounding=product.uom_id.rounding)
                res[product.id] = {
                    'qty_available_detail': qty_available_detail,
                    'qty_available_detail_text': str(qty_available_detail) + _(" On Hand"),
                    'virtual_available_detail': virtual_available_detail
                }
            else:
                res[product.id] = {
                    'qty_available_detail': product.qty_available,
                    'qty_available_detail_text': str(product.qty_available) + _(" On Hand"),
                    'virtual_available_detail': product.virtual_available
                }
        return res
    
    _columns = {
        'qty_available_detail': fields.function(_product_available_detail, multi='qty_available_detail',
                type='float', digits_compute=dp.get_precision('Product Unit of Measure'),
                string='Quantity on Hand'),
        'virtual_available_detail': fields.function(_product_available_detail, multi='qty_available_detail',
                type='float', digits_compute=dp.get_precision('Product Unit of Measure'),
                string='Forcasted Quantity'),
        'qty_available_detail_text': fields.function(_product_available_detail, multi='qty_available_detail',
                type='char'),
    }
    
class product_template(osv.osv):
    _name = "product.template"
    _inherit = "product.template"
    
    def _get_detail_qty(self, x, mod_factor, decimal):
        qty = x * mod_factor
        (mod_qty, res_qty) = divmod(qty, mod_factor)
#         decimal = len(str(int(res_qty)))
        res_qty = res_qty / 10 ** decimal
        y = mod_qty + res_qty
        return y
    
    def _product_available_detail(self, cr, uid, ids, field_names=None, arg=False, context=None):
        res = {}
        for product in self.browse(cr, uid, ids, context):
            if product.uom_id.category_id.detail_qty and product.uom_id.uom_type != 'reference' and product.type != 'service':
                decimal = math.log(product.uom_id.rounding,0.1)
                qty_available_detail = float_round(self._get_detail_qty(product.qty_available, product.uom_id.factor_inv, decimal), precision_rounding=product.uom_id.rounding)
                virtual_available_detail = float_round(self._get_detail_qty(product.virtual_available, product.uom_id.factor_inv, decimal), precision_rounding=product.uom_id.rounding)
                res[product.id] = {
                    'qty_available_detail': qty_available_detail,
                    'qty_available_detail_text': str(qty_available_detail) + _(" On Hand"),
                    'virtual_available_detail': virtual_available_detail
                }
            else:
                res[product.id] = {
                    'qty_available_detail': product.qty_available,
                    'qty_available_detail_text': str(product.qty_available) + _(" On Hand"),
                    'virtual_available_detail': product.virtual_available
                }
        return res
    
    _columns = {
        'qty_available_detail': fields.function(_product_available_detail, multi='qty_available_detail',
                type='float', digits_compute=dp.get_precision('Product Unit of Measure'),
                string='Quantity on Hand'),
        'virtual_available_detail': fields.function(_product_available_detail, multi='qty_available_detail',
                type='float', digits_compute=dp.get_precision('Product Unit of Measure'),
                string='Forcasted Quantity'),
        'qty_available_detail_text': fields.function(_product_available_detail, multi='qty_available_detail',
                type='char'),
    }

class product_uom_categ(osv.osv):
    _inherit = 'product.uom.categ'
    
    _columns = {
        'detail_qty': fields.boolean("Show Detail Qty"),
    }
    
    
