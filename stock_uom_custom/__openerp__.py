{
    "name": "Custom UoM",
    "version": "8.0",
    "depends": ['stock'],
    "author": "Joenan",
    "category": "Stock",
    "description" : """Custom UoM""",
    'data': [
            'views/stock_view.xml'
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
