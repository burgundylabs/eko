# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"
    
        
    qty1 = fields.Float(digits_compute=dp.get_precision('Product Unit of Measure'),
            string='Quantity', default=0.0)
    qty2 = fields.Float(digits_compute=dp.get_precision('Product Unit of Measure'),
            string='Quantity', default=0.0)
    qty3 = fields.Float(digits_compute=dp.get_precision('Product Unit of Measure'),
            string='Quantity', default=0.0)
    uom_id1 = fields.Many2one('product.uom', string='Unit of Measure')
    uom_id2 = fields.Many2one('product.uom', string='Unit of Measure')
    uom_id3 = fields.Many2one('product.uom', string='Unit of Measure')

    @api.multi
    def onchange_product_id(self, pricelist_id, product_id, qty, uom_id, partner_id, date_order=False, fiscal_position_id=False, date_planned=False, name=False, price_unit=False, state='draft'):
        res = super(PurchaseOrderLine, self).onchange_product_id(pricelist_id, product_id, qty, uom_id, partner_id, date_order, fiscal_position_id, date_planned, name, price_unit, state)
        if product_id:
            product = self.env['product.product'].browse(product_id)
            if res.get('value'):
                if product.uom_id1:
                    res['value']['uom_id1'] = product.uom_id1.id
                if product.uom_id2:
                    res['value']['uom_id2'] = product.uom_id2.id
                if product.uom_id3:
                    res['value']['uom_id3'] = product.uom_id3.id
        return res
    
    @api.onchange('qty1')
    def _onchange_qty1(self):
        if self.uom_id1:
            self.product_qty = self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
            if self.uom_id2:
                self.product_qty += self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)
            if self.uom_id3:
                self.product_qty += self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)
        domain = {'uom_id1': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return {'domain': domain}
        
    @api.onchange('uom_id1')
    def _onchange_uom_id1(self):
        if self.uom_id1:
            self.product_qty = self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
        if self.uom_id2:
            self.product_qty += self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)
        if self.uom_id3:
            self.product_qty += self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)
        
    @api.onchange('qty2')
    def _onchange_qty2(self):
        if self.uom_id2:
            self.product_qty = self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)
            if self.uom_id1:
                self.product_qty += self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
            if self.uom_id3:
                self.product_qty += self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)
        domain = {'uom_id2': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return {'domain': domain}
        
    @api.onchange('uom_id2')
    def _onchange_uom_id2(self):
        if self.uom_id2:
            self.product_qty = self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)
        if self.uom_id1:
            self.product_qty += self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
        if self.uom_id3:
            self.product_qty += self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)

    @api.onchange('qty3')
    def _onchange_qty3(self):
        if self.uom_id3:
            self.product_qty = self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)
            if self.uom_id1:
                self.product_qty += self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
            if self.uom_id2:
                self.product_qty += self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)
        domain = {'uom_id3': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return {'domain': domain}
        
    @api.onchange('uom_id3')
    def _onchange_uom_id3(self):
        if self.uom_id3:
            self.product_qty = self.uom_id3._compute_qty_obj(self.uom_id3, self.qty3, self.product_uom)
        if self.uom_id1:
            self.product_qty += self.uom_id1._compute_qty_obj(self.uom_id1, self.qty1, self.product_uom)
        if self.uom_id2:
            self.product_qty += self.uom_id2._compute_qty_obj(self.uom_id2, self.qty2, self.product_uom)

    