{
    "name": "Custom Purchase Order for Advance UoM",
    "version": "8.0",
    "depends": [
                'purchase',
                'product_uom_custom',
    ],
    "author": "Joenan",
    "category": "Purchase",
    "description" : """Custom Purchase Order for Advance UoM""",
    'data': [
            'views/purchase_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
