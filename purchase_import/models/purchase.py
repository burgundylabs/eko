# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    name = fields.Text('Description', required=False)
    price_unit = fields.Float(string='Unit Price', required=False, digits_compute=dp.get_precision('Product Price'))
    date_planned = fields.Date('Scheduled Date', required=False, select=True)
    
    def create(self, cr, uid, vals, context):
        res = super(PurchaseOrderLine, self).create(cr, uid, vals, context)
        po_line = self.pool.get('purchase.order.line').browse(cr, uid, res)
        product = self.pool.get('product.product').browse(cr, uid, vals['product_id'])
        if not vals.get('price_unit', False):
            po_line.price_unit = product.standard_price
        if not vals.get('name', False):
            po_line.name = product.name
        if not vals.get('date_planned', False):
            po_line.date_planned = po_line.order_id.date_order
        return res
    
