# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from openerp.report import report_sxw
from smcus_amount_to_text import amount_to_text_id

class kas_masuk_keluar_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(kas_masuk_keluar_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'no_urut':self._no_urut,
			'amount_to_text':self._amount_to_text,
            'empty_line': self.empty_line,
         })
        self.no=0

    def _no_urut(self):
        self.no+=1
        return self.no

    def _amount_to_text(self, voucher_amount):
        return amount_to_text_id.amount_to_text_in(voucher_amount, False)        

    def empty_line(self, o):
        baris = len(o.line_ids)
        max_baris = 5
        empty = []
        if baris < max_baris:
            add_baris = ''
            a = 1
            selisih = max_baris - baris
            while a < selisih:
                empty.append('.')
                a+=1
        return empty
        
report_sxw.report_sxw(
    'report.laporan.kas.masuk.keluar',
    'account.voucher',
    'addons/fua_account_voucher/report/kas_masuk_keluar_report.rml',
    parser=kas_masuk_keluar_report, header=False
)
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
