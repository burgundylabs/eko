{
    "name": "Report PDF for Invoice in NCR Paper",
    "version": "8.0",
    "depends": ['account',
                'sale_second_discount'
    ],
    "author": "Joenan",
    "category": "Accounting",
    "description" : """Report PDF for Invoice in NCR Paper""",
    'data': [
            'data/paper_data.xml',
            'views/report_invoice.xml',
            'views/invoice_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
