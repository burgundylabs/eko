# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import fields, models, api
from mako.template import Template
from mako.lookup import TemplateLookup
import time
import os
# import locale
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round
# import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _

# Mendefinisikan path dari modul report terkait
tpl_lookup = TemplateLookup(directories=['/Users/joenan/Odoo/custom_addons8/eko/account_report_invoice_ncr'])
 
# Membuat field teks beserta parameternya dan menyimpannya dalam variabel cetak_fields
cetak_fields = {'teks': {'type': 'text', 'default': 'Pencetakan sedang berlangsung.', 'readonly': True }}
 
# Membuat tampilan formnya dari field teks secara simple 
cetak_form = '''<?xml version="1.0"?>
<form string="Cetak ke printer dotmatrix">
    <field name="teks" />
</form>'''

class account_invoice(models.Model):
    _inherit = "account.invoice"
    
    @api.multi
    def cetak(self):
        # Mendefinikan template report berdasarkan path modul terkait 
        tpl = tpl_lookup.get_template('invoice.txt')
        tpl_line = tpl_lookup.get_template('invoice_line.txt')
 
        # Mempersiapkan data-data yang diperlukan                
        user = self.env.user
        order = self
        date = time.strftime('%d/%m/%Y', time.strptime(order.date_invoice, '%Y-%m-%d'))   
                
        no = 0
        rows = []
        for line in order.invoice_line:
            no += 1
            s = tpl_line.render(no=str(no),
                                code=line.product_id.default_code,
                                name=line.product_id.name,
                                qty=str(line.quantity))
            rows.append(s)
         
        # Merender data yang telah disiapkan ke dalam template report text
        s = tpl.render(id=order.name,
                       tgl=order.date_invoice,
                       pelanggan=order.partner_id.name,
                       rows=rows,
                       ref=order.origin,
                       user_name='(' + user.name + ')')
                 
 
        # Membuat temporary file yang akan dicetak beserta pathnya   
        filename = '/Users/joenan/Desktop/%s.txt' % user.id
         
        # Mengisi file tersebut dengan data yang telah dirender
        f = open(filename, 'w')
        f.write(s)
        f.close()
         
        # Proses cetak dijalankan dan pastikan variabel nama_printer adalah nama printer yang anda setting atau tambahkan dengan webmin diatas
        os.system('lpr -Pnama_printer %s' % filename)
         
        # Hapus file yang telah dicetak
        os.remove(filename)
       
        return True
  
    states = {
        'init': {
            'actions': [cetak],
            'result': {'type':'form',
                       'arch':cetak_form,
                       'fields': cetak_fields,
                       'state':[]
                      }
            },
        }
