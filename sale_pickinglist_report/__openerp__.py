{
    'name': 'Picking List Report',
    'version': '8.1.0',
    'category': 'Sale',
    'author': 'Joenan Rayhan',
    'description': """
Picking List Report
==============================================================

""",
    'depends': ['sale','ek_gisindo', 'sale_1step_confirm'],
    'data': [
        'security/ir.model.access.csv',
        'data/picklist_data.xml',
        'views/pickinglist_report_view.xml',
        'views/picklist_view.xml',
        'views/sale_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
