# -*- coding: utf-8 -*-

from datetime import datetime
from openerp import api, models


class ReportSalePickingList(models.AbstractModel):
    _name = 'report.sale_pickinglist_report.report_pickinglist'

    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env['sale.order']
        pick = self.env['picklist.order'].browse(docids)
        data = {'form': {'lines': [], 'invoices': []}}
        data['form']['name'] = pick.name
        lang = self.env['res.lang'].search([('code', '=', self.env.user.lang)])[0]
        data['form']['date'] = datetime.strptime(pick.date, '%Y-%m-%d').strftime(lang.date_format)
        data['form']['date_print'] = datetime.today().strftime(lang.date_format)
        data['form']['so_user_id'] = pick.so_user_id.name
#         data['form']['date'] = date.today().strftime('%d/%m/%Y')
        for order in self.env['picklist.order'].browse(docids).so_ids:
            for line in order.order_line:
                if not line.is_available:
                    continue
                # cek apakah sudah ada produk tsb
                is_same = False
                if data['form'].get('lines'):
                    for data_line in data['form']['lines']:
                        if data_line['product_id'] == line.product_id.id:
                            data_line['qty'] += line.product_uom_qty
                            data_line['weight'] += line.product_id.weight * line.product_uom_qty
                            is_same = True
                            break
                
                if not is_same:
                    data['form']['lines'].append({
                        'product_id': line.product_id.id,
                        'product_code': line.product_id.default_code,
                        'product': line.product_id.name,
                        'qty': line.product_uom_qty,
                        'uom': line.product_uom.name,
                        'weight': line.product_id.weight * line.product_uom_qty,
                    })

            data['form']['invoices'].append({
                'sale': order.name,
                'invoice': order.invoice_ids and order.invoice_ids[0].name or False,
                'partner': order.partner_id.name,
                'partner_type': '-',
                'sales_route': '-',
                'delivery_route': '-',
                'amount': order.amount_total,
            })
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data,
        }
        return self.env['report'].render('sale_pickinglist_report.report_pickinglist', docargs)
