# from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class PicklistOrder(models.Model):
    _name = "picklist.order"
    _description = "Picklist"
      
    @api.one
    @api.depends('so_ids')
    def _check_so_confirmed(self):
        res = True
        for so in self.so_ids:
            if so.state in ('draft','sent'):
                res = False
                self.is_so_confirmed = res
                break
        self.is_so_confirmed = res
            
            
    name = fields.Char(string='Name', required=True, readonly=True, default='/')
    date = fields.Date(string='Date', required=True, default=datetime.today(),
                       states={'draft': [('readonly', False)]})
    user_id = fields.Many2one('res.users', required=True, string='Created by',
                              default=lambda self: self.env.user)
    filter_so_type = fields.Selection([('creator', 'Creator'),
                                       ('Route', 'Route'),
                                       ('Kabupaten', 'Kabupaten'),
                                       ('Kelurahan', 'Kelurahan'),
                                       ('Kecamatan', 'Kecamatan'), ], string='Filter by',
                                      states={'draft': [('readonly', False)]})
    so_user_id = fields.Many2one('res.users', string='Creator', states={'draft': [('readonly', False)]})
    so_route_id = fields.Many2one('master.route', string='Route Customer', states={'draft': [('readonly', False)]})
    so_kabupaten_id = fields.Many2one('master.kabupaten', string='Kabupaten Customer', states={'draft': [('readonly', False)]})
    so_kecamatan_id = fields.Many2one('master.kecamatan', string='Kecamatan Customer', states={'draft': [('readonly', False)]})
    so_kelurahan_id = fields.Many2one('master.kelurahan', string='Kelurahan Customer', states={'draft': [('readonly', False)]})
    so_ids = fields.Many2many('sale.order', 'pickinglist_sale_order', 'picklist_id', 'sale_order_id', required=True,
        string='Sale Order', states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft', 'Draft'),
                              ('confirm', 'Confirm'),
                              ('cancel', 'Cancelled'), ], string='State', readonly=True, default='draft')
    is_so_confirmed = fields.Boolean(string='All SO Confirmed', compute='_check_so_confirmed')
    
    @api.model
    def create(self, vals):
        res = super(PicklistOrder, self).create(vals)
        for pick in res:
            for so_old in pick.so_ids:
                so_old.picklist_id = pick.id
        return res
    
    
    @api.multi
    def write(self, vals):
        sale_obj = self.env['sale.order']
        for pick in self:
            so_ids = vals.get('so_ids', False)
            if so_ids:
                for so_old in pick.so_ids:
                    so_old.picklist_id = False
                for so_new_id in so_ids[0][2]:
                    sale_obj.browse(so_new_id).picklist_id = pick.id
        res = super(PicklistOrder, self).write(vals)
        return res
    
    @api.multi
    def action_confirm(self):
        # check so if picklist_id is true
        for sale in self.so_ids:
            if sale.picklist_id and sale.picklist_id.id != self.id:
                raise osv.except_osv(_('Confirm Failed.'),
                                     _('%s is already used in another picklist.') % sale.name)
        # name
        if self.name == '/':
            context = {'ir_sequence_date': self.date}
            SequenceObj = self.env['ir.sequence']
            st_number = SequenceObj.with_context(context).get('picklist.order')
            self.name = st_number
        # set picklist of each so and set as used
        for sale in self.so_ids:
            sale.picklist_id = self.id
        # set state
        self.state = 'confirm'

    @api.multi
    def action_confirm_all_so(self):
        # confirm all so
        for sale in self.so_ids:
            if sale.state in ('draft','sent'):
                sale.action_button_confirm()

    @api.multi
    def action_cancel(self):
        # set so as unused
        for sale in self.so_ids:
            sale.picklist_id = False
        # set state
        self.state = 'cancel'
        
    @api.multi
    def action_set_draft(self):
        self.state = 'draft'
        
    @api.onchange('so_user_id')
    def _onchange_so_user_id(self):
        if self.so_user_id:
            return {'domain': {'so_ids': [('user_id', '=', self.so_user_id.id),
                                          ('picklist_id', '=', False)]}}
        else:
            return {'domain': {'so_ids': [('picklist_id', '=', False)]}}
    
    @api.onchange('so_route_id')
    def _onchange_so_route_id(self):
        if self.so_route_id:
            return {'domain': {'so_ids': [('route', '=', self.so_route_id.id),
                                          ('picklist_id', '=', False)]}}
        else:
            return {'domain': {'so_ids': [('picklist_id', '=', False)]}}
    
    @api.onchange('so_kabupaten_id')
    def _onchange_so_kabupaten_id(self):
        if self.so_kabupaten_id:
            return {'domain': {'so_ids': [('kabupaten', '=', self.so_kabupaten_id.id),
                                          ('picklist_id', '=', False)]}}
        else:
            return {'domain': {'so_ids': [('picklist_id', '=', False)]}}
    
    @api.onchange('so_kecamatan_id')
    def _onchange_so_kecamatan_id(self):
        if self.so_kecamatan_id:
            return {'domain': {'so_ids': [('kecamatan', '=', self.so_kecamatan_id.id),
                                          ('picklist_id', '=', False)]}}
        else:
            return {'domain': {'so_ids': [('picklist_id', '=', False)]}}
    
    @api.onchange('so_kelurahan_id')
    def _onchange_so_kelurahan_id(self):
        if self.so_kelurahan_id:
            return {'domain': {'so_ids': [('kelurahan', '=', self.so_kelurahan_id.id),
                                          ('picklist_id', '=', False)]}}
        else:
            return {'domain': {'so_ids': [('picklist_id', '=', False)]}}
        
    @api.onchange('filter_so_type')
    def _onchange_filter_so_type(self):
        if self.filter_so_type != 'creator':
            self.so_user_id = False
            
            