# from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
from openerp.osv import osv
import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = "sale.order"
      
    picklist_id = fields.Many2one('picklist.order', string='Picklist', readonly=True, copy=False)
    
    @api.multi
    def action_remove_picklist(self):
        if self.picklist_id.state == 'confirm':
            raise osv.except_osv(_('Error!'),
                                 _('Picklist %s of this Sale Order already confirmed. Please cancel this picklist first.') % self.picklist_id.name)
        
        self.picklist_id = False