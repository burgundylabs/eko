{
    "name": "Total Weight on Hand",
    "version": "8.0",
    "depends": ['stock'],
    "author": "Joenan",
    "category": "Stock",
    "description" : """Total Weight on Hand""",
    'data': [
            'views/stock_view.xml'
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
