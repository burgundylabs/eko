# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    @api.one
    @api.depends('weight','qty_available')
    def _get_total_weight_onhand(self):
        self.weight_onhand = self.weight * self.qty_available
        
    weight_onhand = fields.Float(string='Total Weight on Hand', store=True, readonly=True, compute='_get_total_weight_onhand',)
    
