# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime
from lxml import etree
import math
import pytz
import urlparse

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _

# Master Daerah
class master_propinsi(osv.osv):
    _name="master.propinsi"

    _columns = {
        'name': fields.char('Propinsi', required=True,),
        }

class master_kabupaten(osv.osv):
    _name="master.kabupaten"

    _columns = {
        'name': fields.char('Kabupaten', required=True,),
        "propinsi_id":fields.many2one("master.propinsi", "Propinsi"),
        }

class master_kecamatan(osv.osv):
    _name="master.kecamatan"

    _columns = {
        'name': fields.char('Kecamatan', required=True,),
        "propinsi_id":fields.many2one("master.propinsi", "Propinsi"),
        "kabupaten_id":fields.many2one("master.kabupaten", "Kabupaten"),
        }

class master_kelurahan(osv.osv):
    _name="master.kelurahan"

    _columns = {
        'name': fields.char('Kelurahan', required=True,),
        "propinsi_id":fields.many2one("master.propinsi", "Propinsi"),
        "kecamatan_id":fields.many2one("master.kecamatan", "Kecamatan"),
        "kabupaten_id":fields.many2one("master.kabupaten", "Kabupaten"),
        }

# Master Akta Hak
class master_akta(osv.osv):
    _name="master.akta"

    _columns = {
        'name': fields.char('Akta Hak', required=True,),
        }

# Status Pembayaran
class master_status_pembayaran(osv.osv):
    _name="master.status.pembayaran"

    _columns = {
        'name': fields.char('Status Pembayaran', required=True,),
        }

# Kelengkapan Dokumen
class master_dokumen(osv.osv):
    _name="master.dokumen"

    _columns = {
        'name': fields.char('Dokumen', required=True,),
        }

# Mater Kategori
class master_kategori(osv.osv):
    _name="master.kategori" #Masuk ke master.xml

    _columns = {
        'name': fields.char('Kategori Lahan', required=True,),
        }

# Mater Seksi
class master_seksi(osv.osv):
    _name="master.seksi" #Masuk ke master.xml

    _columns = {
        'name': fields.char('Seksi', required=True,),
        'userids': fields.one2many('master.seksi.detail', 'seksi_id', 'user'),
        }

class master_seksi_detail(osv.osv):
    _name="master.seksi.detail" #Masuk ke master.xml

    _columns = {
        'name': fields.char('Name', required=True,),
        'seksi_id': fields.many2one('master.seksi', 'seksi')    
        }

class master_pemilik_lahan(osv.osv):
    _name="master.pemilik_lahan"

    _columns = {
        'partner_id'    : fields.many2one('Partner'),
        'name': fields.char('Pemilik Lahan', required=True,),
        }

class master_proses_pembebasan_lahan(osv.osv):
    _name="master.proses.pembebasan.lahan"
    _rec_name="sta_id"

    # fungsi prhitungan jumlah bidang
    def _calculated_all(self, cr, uid, ids, name, arg, context=None):
        res = {}
        #import pdb;pdb.set_trace()     
        #looping objek aktif
        for lhn in self.browse(cr, uid, ids):
        # inisialisasi data yg akan dihitung otomatis
            bidang1 = 0
            bidang2 = 0
            tot_panjang_jln = 0
            tot_luas_tanah_kena_tol = 0
            tot_total_ganti_rugi = 0
            tot_luas_tanah_kena_tol2 = 0
            tot_total_ganti_rugi2 = 0
            for x in lhn.partner_id :               
                bidang1 += 1
                tot_panjang_jln += x.panjang_jalan
                tot_luas_tanah_kena_tol += x.luas_tanah_kena_tol
                tot_total_ganti_rugi += x.total_ganti_rugi
                if x.progress == 'bebas':
                    bidang2 += 1
                    tot_luas_tanah_kena_tol2 += x.luas_tanah_kena_tol
                    tot_total_ganti_rugi2 += x.total_ganti_rugi

            self.write(cr,uid,lhn.id,{'bidang1'                 : bidang1,
                                    'bidang2'                   : bidang2,
                                    'tot_panjang_jln'           : tot_panjang_jln,
                                    'tot_luas_tanah_kena_tol'   : tot_luas_tanah_kena_tol,
                                    'tot_total_ganti_rugi'      : tot_total_ganti_rugi,
                                    'tot_luas_tanah_kena_tol2'  : tot_luas_tanah_kena_tol2,
                                    'tot_total_ganti_rugi2'     : tot_total_ganti_rugi2})
            res[lhn.id]=True
        return res

    _columns = {
        'sta_id'        : fields.many2one('master.sta'),
        'partner_id'    : fields.many2many("res.partner", "lahan_partner_rel", "lahan_id", "partner_id", "Proses Pembebasan Lahan"),
        'propinsi'      : fields.many2one("master.propinsi", "Propinsi",required=True),
        'kabupaten'     : fields.many2one("master.kabupaten", "Kabupaten",domain="[('propinsi_id','=',propinsi)]",required=True),
        'kecamatan'     : fields.many2one("master.kecamatan", "Kecamatan",domain="[('kabupaten_id','=',kabupaten)]",required=True),
        'kelurahan'     : fields.many2one("master.kelurahan", "Desa/Kelurahan" ,domain="[('kecamatan_id','=',kecamatan)]",required=True),
        'sta_awal'      : fields.char('STA Awal',required=True),
        'sta_akhir'     : fields.char('STA Akhir',required=True),
        'calculated'    : fields.function(_calculated_all,type="boolean",string="Calculated"),
        'bidang1'       : fields.float('Bidang 1'),
        'bidang2'       : fields.float('Bidang 1'),
        'tot_panjang_jln': fields.integer('Total Panjang Jalan'),
        'tot_luas_tanah_kena_tol': fields.float("Total Luas Tanah Kena Tol"),
        'tot_total_ganti_rugi': fields.float("Total Ganti rugi"),
        'tot_luas_tanah_kena_tol2': fields.float("Total Luas Tanah Kena Tol 2"),
        'tot_total_ganti_rugi2': fields.float("Total Ganti rugi 2"),
        }       
