import datetime
from lxml import etree
import math
import pytz
import urlparse

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _

class res_partner(osv.osv):
	_inherit="res.partner"

	# fungsi untuk mentriger semua field agar dihitung bersamaan
	def compute_all_field(self, cr, uid, ids, name, arg, context=None):
			res = {}
			#import pdb;pdb.set_trace()
			thn = False
			jumlah = 0
			for x in self.browse(cr, uid, ids):
			#Coding nya short by Tahun    
				if x.tgl_ganti_rugi :
					thn = x.tgl_ganti_rugi[:4]
			#Coding Kalkulasi
				harga_tanah_dibayar     = x.luas_tanah_kena_tol * x.harga_tanah_m
				total_ganti_rugi        = harga_tanah_dibayar + x.harga_bangunan + x.harga_tanaman + x.harga_lain
		   

				#untuk menghitung percentasi data partner dg field e_lahan=True
				cr.execute('select count(*) from res_partner where e_lahan = True and active=True')
				jml = cr.fetchone()
				jumlah = 0
				if float(jml[0]):
					jumlah = (1/float(jml[0]))*100

				self.write(cr,uid,x.id,{'harga_tanah_dibayar'   : harga_tanah_dibayar,
										'total_ganti_rugi'      : total_ganti_rugi,
										'tahun'                 : thn,
										'percent'               : jumlah})
				res[x.id]=True
			return res

	_columns = {
		'progres': fields.selection([("belum_bebas", "Belum Bebas"), 
									 ("appraisal", "Appraisal"),
									 ("negosiasi", "Negosiasi"),
									 ("bermasalah", "Bermasalah"),
									 ("bebas", "Bebas")], "Progress", required=True,),
		'progress': fields.selection([("belum_bebas", "Belum Bebas"), 
									 ("appraisal", "Appraisal"),
									 ("negosiasi", "Negosiasi"),
									 ("proses bayar", "Proses Pembayaran"),
									 ("bermasalah", "Bermasalah"),
									 ("bebas", "Bebas")], "Progress", required=True,),
		'seksi': fields.selection([("NA", "N/A"),
									("Seksi_1", "Seksi 1"),
									("Seksi_2", "Seksi 2"),
									("Seksi_3", "Seksi 3"),
									("Seksi_4", "Seksi 4"),
									("Seksi_5", "Seksi 5")], "Seksi", required=True,),
		'tgl_sp2d'          : fields.date("Tgl.SP2D"),
		'no_sp2d'           : fields.char("No.SP2D"),
		'cek_sph'           : fields.char("Cek SPH"),
		'tgl_sph'           : fields.date("Tgl.SPH"),
		'no_sph'            : fields.char("No.SPH"),
		'x_link'            : fields.char("Link Dokumen"),
		'no_dok'            : fields.char("No.Dokumen"),
		'x_ang'             : fields.selection([("apbd_prov", "APBD Provinsi"), 
												("apbd_kota", "APBD Kota Balikpapan")],"Anggaran"),
		# Data Pemilik
		'x_keterangan'      : fields.char('Nama Instansi'),
		'no_ktp'            : fields.char('No.KTP'),
		'masa_berlaku_ktp'  : fields.date("Masa Berlaku"),
		'pekerjaan'         : fields.char("Pekerjaan"),
		'status_perkawinan' : fields.selection([("kawin", "Kawin"), 
												("belum_kawin", "Belum Kawin")],"Status Perkawinan"), 
		'kewarganegaraan'   : fields.selection([("wni", "WNI"), 
												("wna", "WNA")], "Kewarganegaraan"),
		'propinsi'          : fields.many2one("master.propinsi", "Propinsi"),
		'kabupaten'         : fields.many2one("master.kabupaten", "Kabupaten",domain="[('propinsi_id','=',propinsi)]"),
		'kecamatan'         : fields.many2one("master.kecamatan", "Kecamatan",domain="[('kabupaten_id','=',kabupaten)]"),
		'kelurahan'         : fields.many2one("master.kelurahan", "Kelurahan",domain="[('kecamatan_id','=',kecamatan)]"),
		'alamat'            : fields.char("Alamat"),
		'gender'            : fields.selection([("laki", "Laki-Laki"), 
												("perempuan", "Perempuan")], "Jenis Kelamin"),
		# Kelengkapan Dokumen
		# 'dokumen_ids'       : fields.many2one("master.dokumen", "Dokumen"), 
		'dokumen_ids'       : fields.many2many("master.dokumen", "dokumen_partner_rel", "order_id", "master_dokumen", "Dokumen"), 
			 
		# Data Bidang
		'akta_hak'              : fields.many2one("master.akta", "Akta Hak"),
		'no_akta_hak'           : fields.char("No. Akta Hak"),
		'no_peta_bidang'        : fields.char("No. Peta Bidang"),
		'atas_nama_bidang'      : fields.char("Atas Nama Bidang"),
		'sta_awal'              : fields.char('STA Awal'), # tambahan
		'sta_akhir'             : fields.char('STA Akhir'), # tambahan
		'sta'             		: fields.char('STA'),
		'panjang_jalan'         : fields.float("Kilometer (Km)"),
		'bidang_tot'            : fields.integer('Total Bidang'), # tambahan
		'bidang_bayar'          : fields.integer('Bidang Terbayar'), # tambahan
		'longitude'             : fields.integer('Longitude'), # tambahan
		'latitude'              : fields.integer('Latitude'), # tambahan
		# Data Tanah
		'luas_tanah'            : fields.float("Luas Tanah (M2)"),
		'luas_tanah_kena_tol'   : fields.float("Luas Kena Tol (M2)"),
		'harga_tanah_m'         : fields.float("Harga Tanah Per (M2)"),
		'harga_tanah_dibayar'   : fields.float("Harga_Tanah"),
		'harga_bangunan'        : fields.float("Harga_Bangunan"),
		'harga_tanaman'         : fields.float("Harga_Tanaman"),
		'harga_lain'            : fields.float("Lain_Lain"),
		'tgl_ganti_rugi'        : fields.date("Tgl. Ganti Rugi"),
		'status_pembayaran'     : fields.many2one("master.status.pembayaran", "Status Pembayaran"),
		'total_ganti_rugi'      : fields.float("UGR"),
		'permasalahan'          : fields.text("Permasalahan"),
		'pemilik_lahan'         : fields.many2one("master.pemilik_lahan", "Pemilik Lahan"),

		#Spasi
		'x_spasi'               : fields.char("--"),

		# field untuk menghitung otomatis
		'cumpute_all'           : fields.function(compute_all_field,type='boolean',string='Compute'),
		'tahun'                 : fields.char('Tahun'),
		'e_lahan'               : fields.boolean('e-Lahan'),
		'pemilik_lahan_ids'     : fields.one2many('master.pemilik_lahan','partner_id','Nama-Nama Pemilik'),

		# field untuk menghitung percentasi data partner dg field e_lahan=True
		'percent' : fields.float('Percent'),
		}

	#Kode Unik
	_sql_constraints = [('no_dok_uniq', 'unique(no_dok)', 'No.Dokumen Tidak Boleh Sama'),]
	_defaults={"progress":"belum_bebas"}



	 

			
			


