# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'E-Lahan',
    'version': '3.0',
    'category': 'Lahan',
    'sequence': 14,
    'description': """

    """,
    'author': 'EKO H.A',
    'website': 'https://www.solusiaplikasi.wordpress.com',
    'depends': ['base','document','base_report_designer'], # Nama modul yang digunakan pada modul yang dibuat
    'data': ["security/groups.xml",
            "master.xml", 
            "partner.xml",
            "report/sta_report.xml",
            "security/ir.model.access.csv"], # Nama file xml dari modul
    
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
