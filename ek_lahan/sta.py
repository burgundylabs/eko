import datetime
from lxml import etree
import math
import pytz
import urlparse
from datetime import datetime,date

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _

class master_sta(osv.osv):
	_name="master.sta" #Masuk ke master.xml
	_rec_name= "seksi"

	_columns = {
		'seksi'					: fields.many2one('master.seksi', 'Seksi', required=True),
		'sta_awal'				: fields.char('STA Awal'),
		'sta_akhir'				: fields.char('STA Akhir'),
		'penanganan'			: fields.char('Panjang Penanganan'),
		'pengerjaan'			: fields.char('Lahan Yang Dikerjakan'),
		'permasalahanids'		: fields.one2many("master.permasalahan.detail", 'sta_id', 'Permasalahan Seksi'),
		'penanggungjawabids'	: fields.one2many("master.penanggungjawab.detail", 'sta_id', 'penanggungjawab'),
		'lahan_ids'				: fields.one2many("master.proses.pembebasan.lahan", "sta_id", "Proses Pembebasan Lahan"),
		'user_id'				: fields.many2one('res.users','User'),
		'number'				: fields.char('Nomor',size=30),
		'date'  				: fields.date('Tanggal'),	
		}

	_defaults={
		"user_id"	: lambda obj, cr, uid, context: uid,
		"date" 		: date.today().strftime('%Y-%m-%d')
		}		

class master_permasalahan_detail(osv.osv):
	_name="master.permasalahan.detail" #Masuk ke master.xml

	_columns = {
		'name': fields.text('Name', required=True,),
		'sta_id': fields.many2one('master.sta', 'STA')	
		}

class master_penanggungjawab_detail(osv.osv):
	_name="master.penanggungjawab.detail" #Masuk ke master.xml

	_columns = {
		'name': fields.char('Name', required=True,),
		'sta_id': fields.many2one('master.sta', 'Penanggungjawab')	
		}