import time
from openerp.report import report_sxw 

class master_sta(report_sxw.rml_parse): 
    def __init__(self, cr, uid, name, context): 
        super(master_sta, self).__init__(cr, uid, name, context) 
        self.localcontext.update({ 'time': time, }) 
        
report_sxw.report_sxw('report.ek_lahan', 'master.sta', 'addons/ek_lahan/report/sttb.rml', parser=master_sta)