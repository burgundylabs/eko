============================
POS Category wise receipt v9
============================

This module aims to print category wise receipt for point of sale.

Features
========

* The receipt shows category wiseproduct and Subtotal of each category.

Installation
============

Just select it from available modules to install it, there is no need to extra installations.

Configuration
=============

Nothing to configure.


Credits
=======
Developer: Anusha @ cybrosys
Guidance: Nilmar Shereef @ cybrosys, shereef@cybrosys.in
