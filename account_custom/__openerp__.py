{
    "name": "Custom Accounting Module",
    "version": "8.0",
    "depends": ['account_voucher',
                'sale',
    ],
    "author": "Joenan",
    "category": "Accounting",
    "description" : """Custom Accounting Module""",
    'data': [
            'views/account_view.xml',
            'views/res_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
