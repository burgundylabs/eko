# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import fields, models, api
# import locale
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round
# import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    supplier_id = fields.Many2one(related='user_id.supplier_id', store=True, readonly=True, copy=False)
    
