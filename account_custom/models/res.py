# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import fields, models, api
# import locale
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round
# import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _

class ResUsers(models.Model):
    _inherit = 'res.users'
    
    supplier_id = fields.Many2one('res.partner', string='Supplier')
    

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    alamat_npwp = fields.Char(string='Alamat NPWP')