# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import fields, models, api
# import locale
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round
# import openerp.addons.decimal_precision as dp
# from openerp import models, fields, api, _
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class AccountVoucher(models.Model):
    _inherit = "account.voucher"
    
#     @api.one
    def _get_user_id(self):
        context = self._context
        if context is None:
            context = {}
        return context.get('user_id', False)

    user_id = fields.Many2one('res.users', string='Salesperson', default=_get_user_id)
    supplier_id = fields.Many2one(related='user_id.supplier_id', store=True, readonly=True, copy=False)
    

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    supplier_id = fields.Many2one(related='user_id.supplier_id', store=True, readonly=True, copy=False)
    
    @api.multi
    def invoice_pay_customer(self):
        res = super(AccountInvoice, self).invoice_pay_customer()
        res['context']['user_id'] = self.user_id.id
        return res