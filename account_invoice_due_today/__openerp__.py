{
    "name": "Filter Invoice Due Date Today",
    "version": "8.0",
    "depends": ['account','account_invoice_written'],
    "author": "Joenan",
    "category": "Accounting",
    "description" : """Filter Invoice Due Date Today""",
    'data': [
            'views/account_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
