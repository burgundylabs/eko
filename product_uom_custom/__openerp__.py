{
    "name": "Custom Product for Advance UoM",
    "version": "8.0",
    "depends": ['product'],
    "author": "Joenan",
    "category": "Stock",
    "description" : """Custom Product for Advance UoM""",
    'data': [
            'views/product_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
