# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    uom_id1 = fields.Many2one('product.uom', string='UoM 1')
    uom_id2 = fields.Many2one('product.uom', string='UoM 2')
    uom_id3 = fields.Many2one('product.uom', string='UoM 3')
    
