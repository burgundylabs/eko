# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
from openerp import workflow

class sale_order(osv.osv):
    _inherit = 'sale.order'
    _columns = {
        'kabupaten'   : fields.many2one('master.kabupaten', string="Kabupaten", related="partner_id.kabupaten", readonly=True),
        'kecamatan'   : fields.many2one('master.kecamatan', string="Kecamatan", related="partner_id.kecamatan", readonly=True),
        'kelurahan'   : fields.many2one('master.kelurahan', string="Kelurahan", related="partner_id.kelurahan", readonly=True),
        'route'		  : fields.many2one('master.route', string="Kelurahan", related="partner_id.x_route", readonly=True),
        }

