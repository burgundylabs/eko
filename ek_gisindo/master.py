# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime
from lxml import etree
import math
import pytz
import urlparse

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _

# Master Daerah
class master_kabupaten(osv.osv):
    _name="master.kabupaten"
    _columns = {
        'name': fields.char('Kabupaten', required=True,),
        }

class master_kecamatan(osv.osv):
    _name="master.kecamatan"
    _columns = {
        'name': fields.char('Kecamatan', required=True,),
        "kabupaten_id":fields.many2one("master.kabupaten", "Kabupaten"),
        }

class master_kelurahan(osv.osv):
    _name="master.kelurahan"
    _columns = {
        'name': fields.char('Kelurahan', required=True,),
        "kecamatan_id":fields.many2one("master.kecamatan", "Kecamatan"),
        "kabupaten_id":fields.many2one("master.kabupaten", "Kabupaten"),
        }

# Route

class master_route(osv.osv):
    _name="master.route"
    _columns = {
        'name'      : fields.char('Nama', required=True,),
        "keterangan": fields.text("Description"),
        }

_columns = {
        'kabupaten'     : fields.many2one("master.kabupaten", "Kabupaten"),
        'kecamatan'     : fields.many2one("master.kecamatan", "Kecamatan",domain="[('kabupaten_id','=',kabupaten)]"),
        'kelurahan'     : fields.many2one("master.kelurahan", "Kelurahan",domain="[('kecamatan_id','=',kecamatan)]"),
        'route'         : fields.many2one("master.route", "Route"),
        }       
