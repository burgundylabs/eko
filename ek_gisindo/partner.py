import datetime
from lxml import etree
import math
import pytz
import urlparse

import openerp
from openerp import tools, api
from openerp.osv import osv, fields
from openerp.osv.expression import get_unaccent_wrapper
from openerp.tools.translate import _

class res_partner(osv.osv):
    _inherit="res.partner"
    _columns = {
        'chanel'                : fields.selection([("cas_cary", "Cash & Carry"), 
                                     ("convenience_store", "Convenience Store"),
                                     ("drug_store", "Drug Store"),
                                     ("goverment_office", "Government Office / Administration"),
                                     ("hospital_basic_care", "Hospital : Basic Care"),
									 ("hospital_private_general", "Hospital : Private General"),
                                     ("hospital_spesialist", "Hospital : Specialist"),
                                     ("hotel", "Hotel"),
                                     ("maternity_home", "Maternity Home"),
                                     ("midwives", "Midwives"),
                                     ("modern_pharmacy", "Modern Pharmacy"),
                                     ("motel", "Motel"),
                                     ("seasonal_gift_shop", "Seasonal Gift Shop"),
                                     ("self_service_combo", "Self Service Combo"),
                                     ("small_store", "Small Store"),
                                     ("supermarket_big", "Supermarket Big"),
                                     ("supermarket_medium", "Supermarket Medium"),
                                     ("supermarket_small", "Supermarket Small (Minimarket)"),
                                     ("traditional_pharmacy", "Traditional Pharmacy"),
                                     ("wholesaler_with_sales_foce_local", "Wholesaler with Sales Foce Local/Regional"),
                                     ("wholesaler_without_sales_foce_local", "Wholesaler without Sales Foce Local/Regional")],),
        'npwp'                  : fields.char("NPWP"),
        'x_id'		            : fields.char("ID Partner"),
        'kabupaten'             : fields.many2one("master.kabupaten", "Kabupaten"),
        'kecamatan'             : fields.many2one("master.kecamatan", "Kecamatan",domain="[('kabupaten_id','=',kabupaten)]"),
        'kelurahan'             : fields.many2one("master.kelurahan", "Kelurahan",domain="[('kecamatan_id','=',kecamatan)]"),
        'x_route'               : fields.many2one("master.route", "Route"),
        }

#Kode Unik
    _sql_constraints = [('x_id_uniq', 'unique(x_id)', 'ID Partner tidak boleh sama'),]




     

            
            


