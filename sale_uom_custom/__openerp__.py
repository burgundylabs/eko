{
    "name": "Custom Sale Order for Advance UoM",
    "version": "8.0",
    "depends": [
                'sale',
                'product_uom_custom',
    ],
    "author": "Joenan",
    "category": "Sale",
    "description" : """Custom Sale Order for Advance UoM""",
    'data': [
            'views/sale_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
