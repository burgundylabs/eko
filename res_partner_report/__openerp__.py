{
    'name': 'Report Data Warga',
    'version': '8.1.0',
    'category': 'Report',
    'author': 'Joenan Rayhan',
    'description': """
Report Data Warga
==============================================================

""",
    'depends': ['ek_lahan'],
    'data': [
        'views/partner_report_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
