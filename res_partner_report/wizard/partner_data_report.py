from openerp import models, fields, api, _
# from openerp.tools.safe_eval import safe_eval
# from openerp.exceptions import UserError


class PartnerDataReport(models.TransientModel):
    _name = "partner.data.report"
    _description = 'Partner Data Report'
    
    kabupaten_ids = fields.Many2many('master.kabupaten', string='Kabupaten')
    progress = fields.Selection([('all', 'All'),
                                 ('belum_bebas', 'Belum Bebas'),
                                 ("appraisal", "Appraisal"),
                                 ("negosiasi", "Negosiasi"),
                                 ("bermasalah", "Bermasalah"),
                                 ("bebas", "Bebas")], string='Status', required=True, default='all')

    
    @api.multi
    def print_report(self):
        data = {
            'form': {}
        }
#         if self.progress == 'all':
#             for status in ['belum_bebas', 'appraisal', 'negosiasi', 'proses bayar', 'bermasalah', 'bebas']:
#                 data['form']['progress'].append({'name': status, 'partners': self._get_partners()})
#         else:
#             data['form']['progress']['name'] = self.progress
        data['form']['progress'] = self.progress
        data['form']['kabupaten_ids'] = [kabupaten.id for kabupaten in self.kabupaten_ids]
        return self.env['report'].get_action(self, 'res_partner_report.report_partner_data', data=data)
