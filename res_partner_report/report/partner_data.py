# -*- coding: utf-8 -*-

# import time
from openerp import api, models


class ReportPartnerData(models.AbstractModel):
    _name = 'report.res_partner_report.report_partner_data'

    def _get_partners(self, partners):
        partner_res = []
        for partner in partners:
            partner_res.append({
                'name': partner.name,
                'kabupaten': partner.kabupaten.name,
                'kelurahan': partner.kelurahan.name,
                'luastanah': partner.luas_tanah_kena_tol,
                'gantirugi': partner.total_ganti_rugi,
                'progress': partner.progress
            })
        return partner_res


    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        data['form']['lines'] = []
        search_params = []
        kabupaten_param = ('kabupaten', 'in', data['form']['kabupaten_ids']) if data['form'].get('kabupaten_ids', False) else False
        if kabupaten_param:
            search_params.append(kabupaten_param)
        if data['form']['progress'] == 'all':
            for status in ['belum_bebas', 'appraisal', 'negosiasi', 'proses bayar', 'bermasalah', 'bebas']:
                progress_param = [('progress', '=', status)]
                search_partners = self.env['res.partner'].search(search_params + progress_param)
                data['form']['lines'].append({
                    'progress': status,
                    'partners': self._get_partners(search_partners)
                })
        else:
            progress_param = [('progress', '=', data['form']['progress'])]
            search_partners = self.env['res.partner'].search(search_params + progress_param)
            data['form']['lines'].append({
                'progress': data['form']['progress'],
                'partners': self._get_partners(search_partners)
            })
#         import ipdb;ipdb.set_trace()
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data,
        }
        return self.env['report'].render('res_partner_report.report_partner_data', docargs)