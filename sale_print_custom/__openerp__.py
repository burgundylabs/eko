{
    "name": "Custom Report PDF for Sale Order",
    "version": "8.0",
    "depends": ['sale',
                'sale_second_discount'
    ],
    "author": "Joenan",
    "category": "Sales",
    "description" : """Custom Report PDF for Sale Order""",
    'data': [
            'views/report_sale.xml',
            'views/sale_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
