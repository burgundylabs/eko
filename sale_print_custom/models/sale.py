# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

# from openerp.osv import fields, osv
# from openerp.tools.translate import _
# from openerp.tools.float_utils import float_round
# import openerp.addons.decimal_precision as dp
from openerp import models, fields, api, _
# from openerp.account_invoice_written.invoice_written import 
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.one
    @api.depends('amount_total')
    def _get_written(self):
        if any([self.id]):
            invoice = self.env['account.invoice']
            ctx = self._context.copy()
            ctx['res_model'] = 'sale.order'
            if self.currency_id.name == 'IDR':
                written = self.pool.get('account.invoice').action_write(self._cr, self._uid, [self.id], 'id', ctx)
            else:
                written = self.pool.get('account.invoice').action_write(self._cr, self._uid, [self.id], 'en', ctx)
            self.written = written
    
    written = fields.Char(string='Written Amount', compute='_get_written', store=True)
    
    
