# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import api, fields, models, _
# import openerp.addons.decimal_precision as dp
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.one
#     @api.depends('order_line.is_available')
    def _compute_available(self):
        self.total_available = len([line for line in self.order_line if line.is_available == True])
    
    total_available = fields.Integer(string='Total Lines Available', compute='_compute_available')