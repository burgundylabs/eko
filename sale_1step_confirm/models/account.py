from datetime import datetime

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp


class account_invoice(models.Model):
    _inherit = 'account.invoice'
    
    @api.model
    def _default_warehouse(self):
        if self._context.get('default_type') in ('out_invoice', 'out_refund'):
            warehouse_ids = self.env['stock.warehouse'].search([])
            if warehouse_ids:
                return warehouse_ids[0].id
    
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', default=_default_warehouse)
    location_return_id = fields.Many2one('stock.location', string='Return Location')
    
    @api.multi
    def invoice_validate(self):
        if self.type not in ('out_refund','in_refund'):
            return super(account_invoice, self).invoice_validate()
        if self.type == 'out_refund':
            picking_type = self.warehouse_id.in_type_id
            location_id = self.warehouse_id.out_type_id.default_location_dest_id.id
            location_dest_id = self.location_return_id and self.location_return_id.id or self.warehouse_id.lot_stock_id.id
        elif self.type == 'in_refund':
            picking_type = self.warehouse_id.in_type_id
            location_id = self.warehouse_id.lot_stock_id.id
            location_dest_id = self.warehouse_id.in_type_id.default_location_src_id.id
        lines = []
        for line in self.invoice_line:
            if not line.product_id or line.product_id.type == 'service':
                continue
            line_vals = (0, 0, {
                'product_id': line.product_id.id,
                'product_uom_qty': line.quantity,
                'product_uom': line.uos_id.id,
                'name': line.product_id.display_name,
                'location_id': location_id,
                'location_dest_id': location_dest_id
            })
            lines.append(line_vals)
        if lines:
            vals = {
                'partner_id': self.partner_id.id,
                'origin': self.number,
                'move_type': 'one',
                'invoice_state': 'none',
                'picking_type_id': picking_type.id,
                'move_lines': lines
            }
            pick = self.env['stock.picking'].create(vals)
            pick.action_confirm()
            pick.force_assign()
            pick.do_transfer()
            pick.date_done == self.date_invoice
        
        res = super(account_invoice, self).invoice_validate()
        return res
