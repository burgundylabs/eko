# from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp.osv import osv, fields 
import openerp.addons.decimal_precision as dp
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _

class SaleOrderLine(osv.osv):
    _inherit = 'sale.order.line'
    
    _columns = {
        'is_available': fields.boolean(string='Available', copy=False)
    }
    
class SaleOrder(osv.osv):
    _inherit = 'sale.order'
    
    def action_ship_create(self, cr, uid, ids, context=None):
        """Create the required procurements to supply sales order lines, also connecting
        the procurements to appropriate stock moves in order to bring the goods to the
        sales order's requested location.

        :return: True
        """
        context = dict(context or {})
        context['lang'] = self.pool['res.users'].browse(cr, uid, uid).lang
        procurement_obj = self.pool.get('procurement.order')
        sale_line_obj = self.pool.get('sale.order.line')
        for order in self.browse(cr, uid, ids, context=context):
            proc_ids = []
            vals = self._prepare_procurement_group(cr, uid, order, context=context)
            if not order.procurement_group_id:
                group_id = self.pool.get("procurement.group").create(cr, uid, vals, context=context)
                order.write({'procurement_group_id': group_id})

            for line in order.order_line:
                if line.state == 'cancel':
                    continue
                
                # custom
                if not line.is_available:
                    continue
                
                # Try to fix exception procurement (possible when after a shipping exception the user choose to recreate)
                if line.procurement_ids:
                    # first check them to see if they are in exception or not (one of the related moves is cancelled)
                    procurement_obj.check(cr, uid, [x.id for x in line.procurement_ids if x.state not in ['cancel', 'done']])
                    line.refresh()
                    # run again procurement that are in exception in order to trigger another move
                    except_proc_ids = [x.id for x in line.procurement_ids if x.state in ('exception', 'cancel')]
                    procurement_obj.reset_to_confirmed(cr, uid, except_proc_ids, context=context)
                    proc_ids += except_proc_ids
                elif sale_line_obj.need_procurement(cr, uid, [line.id], context=context):
                    if (line.state == 'done') or not line.product_id:
                        continue
                    vals = self._prepare_order_line_procurement(cr, uid, order, line, group_id=order.procurement_group_id.id, context=context)
                    ctx = context.copy()
                    ctx['procurement_autorun_defer'] = True
                    proc_id = procurement_obj.create(cr, uid, vals, context=ctx)
                    proc_ids.append(proc_id)
            # Confirm procurement order such that rules will be applied on it
            # note that the workflow normally ensure proc_ids isn't an empty list
            procurement_obj.run(cr, uid, proc_ids, context=context)

            # if shipping was in exception and the user choose to recreate the delivery order, write the new status of SO
            if order.state == 'shipping_except':
                val = {'state': 'progress', 'shipped': False}

                if (order.order_policy == 'manual'):
                    for line in order.order_line:
                        if (not line.invoiced) and (line.state not in ('cancel', 'draft')):
                            val['state'] = 'manual'
                            break
                order.write(val)
        return True
    
    def action_invoice_create(self, cr, uid, ids, grouped=False, states=None, date_invoice=False, context=None):
        if states is None:
            states = ['confirmed', 'done', 'exception']
        res = False
        invoices = {}
        invoice_ids = []
        invoice = self.pool.get('account.invoice')
        obj_sale_order_line = self.pool.get('sale.order.line')
        partner_currency = {}
        # If date was specified, use it as date invoiced, usefull when invoices are generated this month and put the
        # last day of the last month as invoice date
        if date_invoice:
            context = dict(context or {}, date_invoice=date_invoice)
        for o in self.browse(cr, uid, ids, context=context):
            currency_id = o.pricelist_id.currency_id.id
            if (o.partner_id.id in partner_currency) and (partner_currency[o.partner_id.id] <> currency_id):
                raise osv.except_osv(
                    _('Error!'),
                    _('You cannot group sales having different currencies for the same partner.'))

            partner_currency[o.partner_id.id] = currency_id
            lines = []
            for line in o.order_line:
                if line.invoiced:
                    continue
                # custom
                if not line.is_available:
                    continue
                elif (line.state in states):
                    lines.append(line.id)
            created_lines = obj_sale_order_line.invoice_line_create(cr, uid, lines, context=context)
            if created_lines:
                invoices.setdefault(o.partner_invoice_id.id or o.partner_id.id, []).append((o, created_lines))
        if not invoices:
            for o in self.browse(cr, uid, ids, context=context):
                for i in o.invoice_ids:
                    if i.state == 'draft':
                        return i.id
        for val in invoices.values():
            if grouped:
                res = self._make_invoice(cr, uid, val[0][0], reduce(lambda x, y: x + y, [l for o, l in val], []), context=context)
                invoice_ref = ''
                origin_ref = ''
                for o, l in val:
                    invoice_ref += (o.client_order_ref or o.name) + '|'
                    origin_ref += (o.origin or o.name) + '|'
                    self.write(cr, uid, [o.id], {'state': 'progress'})
                    cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (o.id, res))
                    self.invalidate_cache(cr, uid, ['invoice_ids'], [o.id], context=context)
                # remove last '|' in invoice_ref
                if len(invoice_ref) >= 1:
                    invoice_ref = invoice_ref[:-1]
                if len(origin_ref) >= 1:
                    origin_ref = origin_ref[:-1]
                invoice.write(cr, uid, [res], {'origin': origin_ref, 'name': o.client_order_ref or invoice_ref})
            else:
                for order, il in val:
                    res = self._make_invoice(cr, uid, order, il, context=context)
                    invoice_ids.append(res)
                    self.write(cr, uid, [order.id], {'state': 'progress'})
                    cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (order.id, res))
                    self.invalidate_cache(cr, uid, ['invoice_ids'], [order.id], context=context)
        return res
    
    def action_button_confirm(self, cr, uid, ids, context=None):
        res = super(SaleOrder, self).action_button_confirm(cr, uid, ids, context)
        for sale in self.browse(cr, uid, ids):
            if not sale.amount_total:
                raise osv.except_osv(_('Error!'),
                                     _('The amount total cannot be zero.'))
            # cek line apakah ada yg available
            available = False
            for line in sale.order_line:
                if line.is_available:
                    available = True
            if not available:
                raise osv.except_osv(_('Error!'),
                                     _('Please set at least 1 line as available on %s.') % sale.name)
            # transfer picking
            if sale.picking_ids:
                for pick in sale.picking_ids:
                    pick.action_assign()
                    pick.do_transfer()
                    pick.date_done == datetime.today()
            
            # create and validate invoice
            inv = sale.action_invoice_create(grouped=False, states=None, date_invoice=sale.date_order, context=context)
            if inv:
                sale.invoice_ids[0].signal_workflow('invoice_open')
        return res
    
    def action_check_availability(self, cr, uid, ids, context):
        """Works only for 1 warehouse"""
        for sale in self.browse(cr, uid, ids):
            for line in sale.order_line:
                context['warehouse'] = sale.warehouse_id.id
#                 if line.product_id.with_context(context).virtual_available >= line.product_uom_qty:
                if line.product_id.qty_available >= line.product_uom_qty:
                    line.is_available = True
            
            self._get_amount_bfr_discount(cr, uid, [sale.id], context)
            self._update_picking(cr, uid, ids, context)
#             vals = sale._amount_all(['amount_tax', 'amount_total', 'amount_untaxed'], None)
#             sale.amount_tax = vals[sale.id]['amount_tax']
#             sale.amount_untaxed = vals[sale.id]['amount_untaxed']
#             sale.amount_total = vals[sale.id]['amount_total']
        return True
    
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()
    
    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
        return super(SaleOrder, self)._amount_all_wrapper(cr, uid, ids, field_name, arg, context)

    _columns = {
        'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty', 'is_available'], 10),
            },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Taxes',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty', 'is_available'], 10),
            },
            multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty', 'is_available'], 10),
            },
            multi='sums', help="The total amount."),
    }

    def _update_picking(self, cr, uid, ids, context=None):
        for sale in self.browse(cr, uid, ids):
            for pick in sale.picking_ids:
                if pick:
                    # delete procurement
                    for procurement in pick.group_id.procurement_ids:
                        if procurement.state != 'done':
                            procurement.cancel()
                            procurement.unlink()
                    # delete stock picking
                    if pick.state != 'done':
                        pick.unlink()
            # create stock picking
            sale.action_ship_create()
        return True
     
    def _delete_picking(self, cr, uid, ids, context=None):
        for sale in self.browse(cr, uid, ids):
            for pick in sale.picking_ids:
                if pick:
                    # delete procurement
                    for procurement in pick.group_id.procurement_ids:
                        procurement.cancel()
                        procurement.unlink()
                    # delete stock picking
                    pick.unlink()
        return True
     
    def create(self, cr, uid, vals, context=None):
        '''Create stock picking when create SO'''
        res = super(SaleOrder, self).create(cr, uid, vals, context)
        self.action_ship_create(cr, uid, [res], context)
        return res
     
    def write(self, cr, uid, ids, vals, context=None):
        '''Update stock picking when edit SO'''
        res = super(SaleOrder, self).write(cr, uid, ids, vals, context)
        if vals.get('order_line'):
            self._update_picking(cr, uid, ids, context)
        return res
      
    def unlink(self, cr, uid, ids, context=None):
        self._delete_picking(cr, uid, ids, context)
        return super(SaleOrder, self).action_cancel(cr, uid, ids, context)
      
    def action_cancel(self, cr, uid, ids, context=None):
        self._delete_picking(cr, uid, ids, context)
        return super(SaleOrder, self).action_cancel(cr, uid, ids, context)
    
    def action_confirm_multi(self, cr, uid, ids, context=None):
        for sale in self.browse(cr, uid, ids):
            self.action_button_confirm(cr, uid, [sale.id], context)
        return True
    
    
