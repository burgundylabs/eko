# from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp.osv import osv, fields 
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID, api
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _

JOURNAL_TYPE_MAP = {
    ('outgoing', 'customer'): ['sale'],
    ('outgoing', 'supplier'): ['purchase_refund'],
    ('outgoing', 'transit'): ['sale', 'purchase_refund'],
    ('incoming', 'supplier'): ['purchase'],
    ('incoming', 'customer'): ['sale_refund'],
    ('incoming', 'transit'): ['purchase', 'sale_refund'],
}

# class StockPicking(osv.osv):
#     _inherit = 'stock.picking'
#     
#     @api.cr_uid_ids_context
#     def do_transfer(self, cr, uid, picking_ids, context=None):
#         res = super(StockPicking, self).do_transfer(cr, uid, picking_ids, context)
#         # create and validate invoice
#         for pick_id in picking_ids:
#             if self.browse(cr, uid, pick_id).invoice_state == '2binvoiced':
#                 picking_pool = self.pool.get('stock.picking')
#                 journal_ids = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'SAJ')])
#                 
#                 pick = picking_pool.browse(cr, uid, pick_id)
#                 type = pick.picking_type_id.code
#                 usage = pick.move_lines[0].location_id.usage if type == 'incoming' else pick.move_lines[0].location_dest_id.usage
#                 journal_type = JOURNAL_TYPE_MAP.get((type, usage), ['sale'])[0]
#                 journal2type = {'sale':'out_invoice', 'purchase':'in_invoice' , 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
#                 inv_type = journal2type.get(journal_type) or 'out_invoice'
#         
#                 inv = picking_pool.action_invoice_create(cr, uid, pick_id,
#                   journal_id=journal_ids[0],
#                   group=False,
#                   type=inv_type,
#                   context=context)
# #                 import ipdb;ipdb.set_trace()
#                 if inv:
#                     inv.signal_workflow('invoice_open')
#         return res
    
class stock_return_picking(osv.osv_memory):
    _inherit = 'stock.return.picking'
    
    def _create_returns(self, cr, uid, ids, context=None):
        res = super(stock_return_picking, self)._create_returns(cr, uid, ids, context)
        data = self.pool.get('stock.return.picking').browse(cr, uid, ids)
        # transfer picking
        if res[0]:
            pick = self.pool.get('stock.picking').browse(cr, uid, res[0])
            pick.action_assign()
            pick.do_transfer()
            pick.date_done == datetime.today()
        
            # create invoice
            if data.invoice_state == '2binvoiced':
                picking_pool = self.pool.get('stock.picking')
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'SAJ')])
                
                pick = picking_pool.browse(cr, uid, res[0])
                type = pick.picking_type_id.code
                usage = pick.move_lines[0].location_id.usage if type == 'incoming' else pick.move_lines[0].location_dest_id.usage
                journal_type = JOURNAL_TYPE_MAP.get((type, usage), ['sale'])[0]
                journal2type = {'sale':'out_invoice', 'purchase':'in_invoice' , 'sale_refund':'out_refund', 'purchase_refund':'in_refund'}
                inv_type = journal2type.get(journal_type) or 'out_invoice'
        
                inv = picking_pool.action_invoice_create(cr, uid, res[0],
                  journal_id=journal_ids[0],
                  group=False,
                  type=inv_type,
                  context=context)
                
                # validate invoice
                if inv:
                    self.pool.get('account.invoice').browse(cr, uid, inv).signal_workflow('invoice_open')
        return res
        
