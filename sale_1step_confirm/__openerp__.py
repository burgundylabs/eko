{
    "name": "1 Step Confirm of Sale Order",
    "version": "8.0",
    "depends": [
        'sale_stock',
        'ek_gisindo'
    ],
    "author": "Joenan",
    "category": "Sales",
    "description" : """1 Step Confirm of Sale Order""",
    'data': [
            'data/sale_workflow.xml',
            'views/sale_view.xml',
            'views/account_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
