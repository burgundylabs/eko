{
    "name": "Product Supplier",
    "version": "8.0",
    "depends": ['product'],
    "author": "Joenan",
    "category": "Stock",
    "description" : """Product Supplier""",
    'data': [
            'views/product_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
