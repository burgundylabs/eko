# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.v7
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                if line.is_available:
                    val1 += line.price_subtotal
                    val += self._amount_line_tax(cr, uid, line, context=context)
            res[order.id]['amount_untaxed'] = val1 * (1 - (order.customer_discount_total or 0.0) / 100.0)
            res[order.id]['amount_tax'] = val * (1 - (order.customer_discount_total or 0.0) / 100.0)
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
        return res
    
    @api.multi
    @api.depends('order_line.product_uom_qty', 'order_line.price_unit')
    def _get_amount_bfr_discount(self):
        for order in self:
            total = 0
            for line in order.order_line:
                if line.is_available:
                    total += line.product_uom_qty * line.price_unit
            order.amount_bfr_discount = total

    
    @api.multi
    @api.depends('amount_bfr_discount', 'amount_untaxed')
    def _get_discount_total(self):
        """
        Compute the total discount of the SO.
        """
        for order in self:
            order.discount_total = order.amount_bfr_discount - order.amount_untaxed