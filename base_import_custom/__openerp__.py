{
    "name": "Custom Import by Certain Field",
    "version": "8.0",
    "depends": [
        'base',
        'ek_gisindo'
    ],
    "author": "Joenan",
    "category": "Extra",
    "description" : """Custom Import by Certain Field""",
    'data': [
        "views/res_users.xml"
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
