{
    "name": "Second Discount for Sale Order",
    "version": "8.0",
    "depends": [
        'stock',
        'sale',
#         'ek_gisindo'
    ],
    "author": "Joenan",
    "category": "Sales",
    "description" : """Second Discount for Sale Order""",
    'data': [
            'security/ir.model.access.csv',
            'views/sale_view.xml',
            'views/invoice_view.xml',
            'views/product_view.xml',
            'views/partner_view.xml',
            'views/user_view.xml',
            'views/sale_promo_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
