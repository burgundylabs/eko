# from datetime import date
import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.multi
#     @api.depends('discount_total')
    @api.depends('order_line.product_uom_qty', 'order_line.price_unit')
    def _get_amount_bfr_discount(self):
        for order in self:
            total = 0
            for line in order.order_line:
                total += line.product_uom_qty * line.price_unit
            order.amount_bfr_discount = total
#             order.amount_bfr_discount = order.amount_untaxed + order.discount_total 
         
    @api.multi
#     @api.depends('amount_untaxed', 'customer_discount_total', 'order_line.discount_primary', 'order_line.discount_secondary')
    @api.depends('amount_bfr_discount', 'amount_untaxed')
    def _get_discount_total(self):
        """
        Compute the total discount of the SO.
        """
        for order in self:
#             amount_discount = 0.0
#             for line in order.order_line:
#                 base_price = line.price_unit * line.product_uom_qty
#                 amount_discount += base_price * line.discount_primary / 100
#                 amount_discount += base_price * (1 - (line.discount_primary or 0.0) / 100.0) * line.discount_secondary / 100
#             amount_discount += amount_discount * order.customer_discount_total / 100.0
#             order.discount_total = amount_discount

#                 amount_discount += (line.price_unit * line.product_uom_qty) - line.price_subtotal
            order.discount_total = order.amount_bfr_discount - order.amount_untaxed
    
    amount_bfr_discount = fields.Float(string='Before Discount', readonly=True, store=True,
        digits=dp.get_precision('Product Price'), compute='_get_amount_bfr_discount')
    discount_total = fields.Float(string='Discount Amount', readonly=True, store=True,
        digits=dp.get_precision('Product Price'), compute='_get_discount_total')
    promo_ids = fields.Many2many('sale.promo', 'sale_promo_order_rel', 'order_id', 'promo_id', string='Promotions')
    customer_discount_total = fields.Float(string='Cust. Discount')
    date_due = fields.Date(string='Due Date')
    
    @api.multi
    def write(self, vals):
        if not vals.get('date_due'):
            if vals.get('payment_term', 'null') != 'null':
                payment_term = self.env['account.payment.term'].browse(vals.get('payment_term'))
            elif not vals.get('payment_term', 'null'):
                vals['date_due'] = False
            else:
                payment_term = self.payment_term
            
            if payment_term:
                pterm_list = payment_term.with_context(currency_id=self.company_id.currency_id.id).compute(value=1, date_ref=self.date_order.split(' ')[0])[0]
                vals['date_due'] = max(payment_line[0] for payment_line in pterm_list)
            else:
                vals['date_due'] = False
        res = super(SaleOrder, self).write(vals)
        return res
    
    @api.v7
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = super(SaleOrder, self)._amount_all(cr, uid, ids, field_name, arg, context)
        for order in self.browse(cr, uid, ids):
            res[order.id]['amount_untaxed'] = res[order.id]['amount_untaxed'] * (1 - (order.customer_discount_total or 0.0) / 100.0)
            res[order.id]['amount_tax'] = res[order.id]['amount_tax'] * (1 - (order.customer_discount_total or 0.0) / 100.0)
            res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
        return res
    
    @api.v7
    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        res = super(SaleOrder, self).onchange_partner_id(cr, uid, ids, part, context)
        if part:
            for sale in self.browse(cr, uid, ids):
                partner = self.pool.get('res.partner').browse(cr, uid, part)
                res['value']['customer_discount_total'] = partner.discount_sale_total
                for line in sale.user_id.discount_line:
                    if line.customer_id.id == sale.partner_id.id:
                        res['value']['payment_term'] = line.payment_term_id.id
                        break
                    else:
                        res['value']['payment_term'] = False
        else:
            res['value']['payment_term'] = False
            res['value']['date_due'] = False
        return res
    
    @api.model
    def _prepare_invoice(self, order, lines):
        res = super(SaleOrder, self)._prepare_invoice(order, lines)
        res['customer_discount_total'] = order.customer_discount_total
        return res
    
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.multi
    @api.depends('order_id.partner_id', 'order_id.user_id')
    def _get_discount_secondary(self):
        for line in self:
            if line.order_id.partner_id:
#                 line.discount_secondary = line.order_id.partner_id.discount_sale
                for user_discount_line in line.order_id.user_id.discount_line:
                    if line.order_id.partner_id == user_discount_line.customer_id:
                        line.discount_secondary = user_discount_line.discount
            else:
                line.discount_secondary = 0
     
    @api.multi
    @api.depends('order_id.promo_ids', 'product_id', 'product_uom_qty', 'order_id.date_order')
    def _get_discount(self):
        for line in self:
            discount_promo, discount_qty = 0.0, 0.0
            # count all promo discount
#             if line.order_id.promo_ids:
#                 for promo in line.order_id.promo_ids:
#                     discount_promo += promo.discount
            # qty discount old
#             if line.product_uom_qty >= line.product_id.discount_min_qty:
#                 discount_qty = line.product_id.discount_qty
            
            # qty discount
            for partner_discount_line in line.order_id.partner_id.discount_line:
                if (line.product_id == partner_discount_line.product_id or \
                (line.product_id.categ_id.parent_left >= partner_discount_line.categ_id.parent_left and 
                 line.product_id.categ_id.parent_right <= partner_discount_line.categ_id.parent_right)) and \
                 line.product_uom_qty >= partner_discount_line.min_qty:
                    if not partner_discount_line.date_end or line.order_id.date_order <= partner_discount_line.date_end:
                        discount_qty = partner_discount_line.discount
            
            line.discount_primary = discount_qty
    
    @api.multi
    @api.depends('price_unit', 'product_uom_qty')
    def _get_dpp(self):
        for line in self:
            line.dpp = line.price_unit * line.product_uom_qty
    
    @api.multi
    @api.depends('dpp', 'price_subtotal')
    def _get_discount_total(self):
        """
        Compute the total discount of the line.
        """
        for line in self:
            line.discount_total = line.dpp - line.price_subtotal
    
    discount_primary = fields.Float(string='Discount (%)', readonly=True, compute=_get_discount, store=True,
        help='Product Discount + Import Discount')
    discount_secondary = fields.Float(string='Cust. Discount (%)', compute=_get_discount_secondary, store=True)
    import_discount = fields.Float(string='Import Discount (%)')
    dpp = fields.Float(string='DPP', readonly=True, compute=_get_dpp, store=True,
        help='Qty x unit price')
    discount_total = fields.Float(string='Discount Amount', readonly=True, store=True,
        digits=dp.get_precision('Product Price'), compute='_get_discount_total')
    
    @api.model
    def _calc_line_base_price(self, line, context=None):
        res = super(SaleOrderLine, self)._calc_line_base_price(line, context={})
        res = res * (1 - (line.discount_secondary or 0.0) / 100.0) * (1 - ((line.discount_primary + line.import_discount) or 0.0) / 100.0)
        return res
    
    @api.model
    def _prepare_order_line_invoice_line(self, line, account_id=False):
        res = super(SaleOrderLine, self)._prepare_order_line_invoice_line(line, account_id)
        res['discount'] = line.discount_primary
        res['discount_secondary'] = line.discount_secondary
        res['import_discount'] = line.import_discount
        return res
    
