# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SalePromo(models.Model):
    _name = 'sale.promo'
    _description = 'Promotion'
    
    name = fields.Char(string='Name', required=True)    
    discount = fields.Float(string='Discount')    
