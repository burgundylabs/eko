# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ResUsers(models.Model):
    _inherit = 'res.users'
    
    discount_line = fields.One2many('user.discount.line', 'user_id', string='Discount Line')
    
class UserDiscountLine(models.Model):
    _name = 'user.discount.line'
    _description = 'Discount Line'
    
    user_id = fields.Many2one('res.users', string='User')
    customer_id = fields.Many2one('res.partner', string='Customer')
    discount = fields.Float(string='Discount (%)')
    payment_term_id = fields.Many2one('account.payment.term', string='Payment Term')
    
