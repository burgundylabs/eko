# from datetime import date
# from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from openerp import models, fields, api, _
# import openerp.addons.decimal_precision as dp
# from openerp.tools.misc import formatLang
# from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    discount_sale = fields.Float(string='Sale Discount')
    discount_sale_total = fields.Float(string='Sale Discount Total')
    discount_line = fields.One2many('partner.discount.line', 'partner_id', string='Discount Line')
    
class PartnerDiscountLine(models.Model):
    _name = 'partner.discount.line'
    _description = 'Discount Line'
    
    partner_id = fields.Many2one('res.partner', string='Partner')
    product_id = fields.Many2one('product.product', string='Product')
    categ_id = fields.Many2one('product.category', string='Category')
    min_qty = fields.Float(string='Minimum Qty')
    discount = fields.Float(string='Discount (%)')
    date_end = fields.Date(string='End Date')
    
